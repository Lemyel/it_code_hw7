from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('about_site', views.AboutList.as_view()),
    path('about_author/<int:pk>', views.AuthorDetail.as_view()), 
    path('book/<int:pk>', views.BookDetail.as_view()), 
]