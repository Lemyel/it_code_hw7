from django.shortcuts import render
from books import models
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView, TemplateView

# Create your views here.

class TitleMixin():
    title = None

    def get_title(self):
        return self.title
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.get_title()
        return context

class BookDetail(TitleMixin, DetailView):
    model = models.Book
    template_name = 'books/book.html'
    context_object_name = 'book'
    title = "Содержание книги"

# class IndexTemplate(TitleMixin, ListView):
#     template_name = 'books/index.html'
#     title = "Главная страница"
#     model = models.Book
#     context_object_name = 'books'

def index(request):
    model = models.Book.objects.all()
    title = "Главная страница"
    context = { 
            "books":model,
            "title":title
            }
    return render(request, 'books/index.html', context=context)

class AuthorDetail(TitleMixin, DetailView):
    model = models.BookAuthor
    template_name = 'books/about_author.html'
    context_object_name = 'author'
    title = 'Информация об авторе'

# def get_author(request , pk):
#     # author = models.BookAuthor.objects.get(id=pk)
#     author = get_object_or_404(models.BookAuthor, pk=pk)
#     context = {"author": author}
#     return render(request, 'books/about_author.html', context=context)

class AboutList(TitleMixin, ListView):
    template_name = 'books/about_site.html'
    title = 'О вебсайте'
    model = models.Book

# def about(request):
#     return render(request, 'books/about_site.html')


